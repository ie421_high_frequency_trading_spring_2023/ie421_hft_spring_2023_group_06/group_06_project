FROM python:3.10-bullseye

ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get install -y \
  build-essential \
  libcairo2-dev \
  cargo \
  libfreetype6-dev \
  gcc \
  libgdk-pixbuf2.0-dev \
  gettext \
  libjpeg-dev \
  liblcms2-dev \
  libffi-dev \
  musl-dev \
  libopenjp2-7-dev \
  libssl-dev \
  libpango1.0-dev \
  poppler-utils \
  postgresql-client \
  libpq-dev \
  python3-dev \
  rustc \
  tcl-dev \
  libtiff5-dev \
  tk-dev \
  zlib1g-dev

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN rustup update

RUN pip3 install cryptography
COPY ./ /DiscordBot
WORKDIR /DiscordBot
RUN pip3 install -r requirements.txt

# Install Go
RUN curl -O https://dl.google.com/go/go1.17.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.17.linux-amd64.tar.gz
RUN rm go1.17.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin

# Install ollama dependencies
RUN apt-get install -y cmake

# Clone and build ollama
RUN git clone git@github.com:jmorganca/ollama.git /ollama
WORKDIR /ollama
RUN go generate ./...
RUN go build .

# Start the server
CMD ["./ollama", "serve"]

CMD ["./ollama", "run", "llama2:13b"]

