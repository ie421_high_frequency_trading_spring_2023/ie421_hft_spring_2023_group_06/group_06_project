# group_06_project

## Members

Saranpat Prasertthum (**sp73@illinois.edu**)

Ishan Shah (**ishah9@illinoi.edu**)

Ritik Dutta (**ritikd2@illinois.edu**)

# ChatGPT Discord Bot

## Features

* `/chat [message]` Chat with ChatGPT!
* **`/plot [message]`:** Generate a plot according to the prompt.
* **`/code [message]`:** Generate verified code.
* **`/sec [message]`** Asks questions on company's 10-K filings

### Chat

![image](https://user-images.githubusercontent.com/89479282/206497774-47d960cd-1aeb-4fba-9af5-1f9d6ff41f00.gif)

# Setup

## Critical prerequisites to install

* run ``pip3 install -r requirements.txt``
* **Rename the file `.env.dev` to `.env`**
* Recommended python version `3.10`

## Step 1: Create a Discord bot

1. Go to https://discord.com/developers/applications create an application
2. Build a Discord bot under the application
3. Get the token from bot setting

   ![image](https://user-images.githubusercontent.com/89479282/205949161-4b508c6d-19a7-49b6-b8ed-7525ddbef430.png)
4. Store the token to `.env` under the `DISCORD_BOT_TOKEN`

   <img height="190" width="390" alt="image" src="https://user-images.githubusercontent.com/89479282/222661803-a7537ca7-88ae-4e66-9bec-384f3e83e6bd.png">
5. Turn MESSAGE CONTENT INTENT `ON`

   ![image](https://user-images.githubusercontent.com/89479282/205949323-4354bd7d-9bb9-4f4b-a87e-deb9933a89b5.png)
6. Invite your bot to your server via OAuth2 URL Generator

   ![image](https://user-images.githubusercontent.com/89479282/205949600-0c7ddb40-7e82-47a0-b59a-b089f929d177.png)

## Step 2: Official API authentication

### Generate an OpenAI API key

1. Go to https://beta.openai.com/account/api-keys
2. Click Create new secret key

   ![image](https://user-images.githubusercontent.com/89479282/207970699-2e0cb671-8636-4e27-b1f3-b75d6db9b57e.PNG)
3. Store the SECRET KEY to `.env` under the `OPENAI_API_KEY`
4. You're all set for [Step 3](#step-3-run-the-bot-on-the-desktop)

## Step 3(Vagrant): Run the bot with Vagrant

1. Run the Project using Vagrant `vagrant up`
2. Inspect whether the bot is running `vagrant status`
3. Inspect whether the bot works well `vagrant ssh` and `sudo tail -f /var/log/chatgpt-discord-bot.log`
4. To actively look into execution:
   - `vagrant ssh` to ssh into the VM 
   - `cd /DiscordBot` to get to the code repo
   - `sudo python3 main.py` to explicitly start the bot. Note that doing this is not necessary, `main.py` is run by default within the provisioning script. 

## Step 3: Run the bot on the desktop [Optional]

1. Open a terminal or command prompt
2. Navigate to the directory where you installed the ChatGPT Discord bot
3. Run `python3 main.py` or `python main.py` to start the bot

### Have a good chat!

## Optional: Disable logging

* Set the value of `LOGGING` in the `.env` to False

## Optional: Setup system prompt

* A system prompt would be invoked when the bot is first started or reset
* You can set it up by modifying the content in `system_prompt.txt`
* All the text in the file will be fired as a prompt to the bot
* Get the first message from ChatGPT in your discord channel!

  1. Right-click the channel you want to recieve the message, `Copy  ID`

     ![channel-id](https://user-images.githubusercontent.com/89479282/207697217-e03357b3-3b3d-44d0-b880-163217ed4a49.PNG)
  2. paste it into `.env` under `DISCORD_CHANNEL_ID`

---

 Credit: [**中文說明**](https://zero6992.github.io/2023/03/09/chatGPT-discord-bot-chinese/)
