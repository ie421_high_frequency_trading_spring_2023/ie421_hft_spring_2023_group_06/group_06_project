import pandas as pd
import json
import requests
import numpy as np
import openai
import json
import os
from dotenv import load_dotenv
load_dotenv()
import regex as re
import tiktoken
from bs4 import BeautifulSoup
from langchain.text_splitter import TokenTextSplitter
import time
from langchain.llms import Ollama
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler  


llm = Ollama(base_url="http://0.0.0.0:11435", model="llama2:13b")  # http://localhost:11434

model = 'gpt-4'
#model = "gpt-3.5-turbo"


def get_xbrl_gaap(cik):
    cik = str(cik)
    cik = cik.zfill(10)
    response = requests.get('https://data.sec.gov/api/xbrl/companyfacts/CIK' + cik + '.json', headers=headers)
    if response.status_code == 200:
        return response.json()['facts']['us-gaap']
    else:
        return 'Invalid response'

def convert_idx_to_df(url):
    headers = {'User-Agent': "email@address.com"}

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        file = io.BytesIO(response.content)
        names = ['Company Name','Form Type','CIK','Date Filed','File Name']
        df = pd.read_fwf(file, colspecs=[(0,61),(62,74),(74,84),(86,94),(98,146)], names=names, skiprows=11)
        return df
    else:
        return 'Invalid URL'

def remove_tags(text):
    prev_close_tag = 0
    new_text = ''
    for i in range(len(text)):
        if text[i] == '<':
            new_text = new_text + text[prev_close_tag+1:i]
        elif text[i] == '>':
            prev_close_tag = i
    return new_text
   

def get_company_filing(acc_num, cik):
    headers = {'User-Agent': "email@address.com"}
    response = requests.get('https://www.sec.gov/Archives/edgar/data/' + str(cik) + '/' + str(acc_num) + '.txt', headers=headers)
    if response.status_code == 200:
        return str(response.text)

def find_cik(ticker, title):
    with open('./company_tickers.json', 'r') as f:
        ciks = json.load(f)
        for key in ciks.keys():
            if ciks[key]['ticker'] == ticker or title.lower() in ciks[key]['title'].lower():
                return ciks[key]['cik_str']
        return 'OP FAILED'

def get_filing_history(cik):
    headers = {'User-Agent': "email@address.com"}
    cik = str(cik)
    cik = cik.zfill(10)
    response = requests.get('https://data.sec.gov/api/xbrl/companyfacts/CIK' + cik + '.json', headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        return 'Invalid response'
        

#def read_file(suffix):
#    assert '.txt' in suffix, "Invalid argument"
#    headers = {'User-Agent': "email@address.com"}
#    response = requests.get('https://www.sec.gov/Archives/' + suffix, headers=headers)
#    if response.status_code == 200:

def remove_unicode(text):
    text = text.replace("&nbsp", "")
    text = text.replace("&#8217;", "'")
    text = text.replace("&#8212;", "---")
    text = text.replace("&#160;", " ")
    text = text.replace("&#8221;", '"')
    text = text.replace("&#8220;", '"')
    text = text.replace("&#47;", "/")
    text = text.replace("&#58;", ":")
    text = text.replace("\\n", " ")
    text = text.replace("\n", "")
    text = text.replace("\xa0", " ")
    text = text.replace("\\t", "        ")
    return text
            
def get_10k_text(text, cik, company):
    # Regex to find <DOCUMENT> tags
    doc_start_pattern = re.compile(r'<DOCUMENT>')
    doc_end_pattern = re.compile(r'</DOCUMENT>')
    # Regex to find <TYPE> tag prceeding any characters, terminating at new line
    type_pattern = re.compile(r'<TYPE>[^\n]+')

    # Create 3 lists with the span idices for each regex

    ### There are many <Document> Tags in this text file, each as specific exhibit like 10-K, EX-10.17 etc
    ### First filter will give us document tag start <end> and document tag end's <start> 
    ### We will use this to later grab content in between these tags
    doc_start_is = [x.end() for x in doc_start_pattern.finditer(text)]
    doc_end_is = [x.start() for x in doc_end_pattern.finditer(text)]
    
    ### Type filter is interesting, it looks for <TYPE> with Not flag as new line, ie terminare there, with + sign
    ### to look for any char afterwards until new line \n. This will give us <TYPE> followed Section Name like '10-K'
    ### Once we have have this, it returns String Array, below line will with find content after <TYPE> ie, '10-K' 
    ### as section names
    doc_types = [x[len('<TYPE>'):] for x in type_pattern.findall(text)]

    document = {}

    # Create a loop to go through each section type and save only the 10-K section in the dictionary
    for doc_type, doc_start, doc_end in zip(doc_types, doc_start_is, doc_end_is):
        #print(doc_type)
        if doc_type == '10-K':
            document[doc_type] = text[doc_start:doc_end]
        elif 'EX-' in doc_type: # figure out issue with exhibits later
            try:
                document['EXHIBITS'] = document['EXHIBITS'] + text[doc_start:doc_end]
            except:
                #print('hereh')
                document['EXHIBITS'] = text[doc_start:doc_end]

    # Write the regex
    regex = regex = re.compile(r'(>(Item|ITEM)(\s|&#160;|&nbsp;)(1|1A|2|3|4|5|6|1B|7A|7|8|9|10|11|12|13|14|15)\.{1})') #re.compile(r'(>(Item|ITEM)(\s|&#160;|&nbsp;)(1(\s|&#160;|&nbsp;)|1A|2|3|4|5|6|1B|7A|7|8|9|10|11|12|13|14|15)\.{0,1})|(ITEM\s(1(\s|&#160;|&nbsp;)|1A|2|3|4|5|6|1B|7A|7|8|9|10|11|12|13|14|15))')

    # Matches
    #print(text)
    matches = regex.finditer(document['10-K'])
    
    # Create the dataframe
    test_df = pd.DataFrame([(x.group(), x.start(), x.end()) for x in matches])
    
    test_df.columns = ['item', 'start', 'end']
    test_df['item'] = test_df.item.str.lower()

    # Get rid of unnesesary charcters from the dataframe
    test_df.replace('&#160;',' ',regex=True,inplace=True)
    test_df.replace('&nbsp;',' ',regex=True,inplace=True)
    test_df.replace(' ','',regex=True,inplace=True)
    test_df.replace('\.','',regex=True,inplace=True)
    test_df.replace('>','',regex=True,inplace=True)

    # Drop duplicates
    pos_dat = test_df.sort_values('start', ascending=True).drop_duplicates(subset=['item'], keep='last')

    # Set item as the dataframe index
    pos_dat.set_index('item', inplace=True)

    # Get Item 1a
    raw_items = {}
    #print(pos_dat)

    sections = ['1', '1A', '1B', '2', '3', '4', '5', '6', '7', '7A', '8', '9', '10', '11', '12', '13', '14', '15']
    for i in range(len(sections)-1):
        try:
            raw_items['Item ' + sections[i]] = document['10-K'][pos_dat['start'].loc['item' + sections[i].lower()]:pos_dat['start'].loc['item' + sections[i+1].lower()]]
        except Exception as e:
            raw_items['Item ' + sections[i]] = 'SECTION UNAVAILABLE'
            print(e)
            with open('./error.txt', "w") as f:
                f.write(str(e))
                f.write('\n')
    """try:
        raw_items['Item 1'] = document['10-K'][pos_dat['start'].loc['item1']:pos_dat['start'].loc['item1a']]
        raw_items['Item 1A'] = document['10-K'][pos_dat['start'].loc['item1a']:pos_dat['start'].loc['item1b']]
        raw_items['Item 1B'] = document['10-K'][pos_dat['start'].loc['item1b']:pos_dat['start'].loc['item2']]
        raw_items['Item 2'] = document['10-K'][pos_dat['start'].loc['item2']:pos_dat['start'].loc['item3']]
        raw_items['Item 3'] = document['10-K'][pos_dat['start'].loc['item3']:pos_dat['start'].loc['item4']]
        raw_items['Item 4'] = document['10-K'][pos_dat['start'].loc['item4']:pos_dat['start'].loc['item5']]
        raw_items['Item 5'] = document['10-K'][pos_dat['start'].loc['item5']:pos_dat['start'].loc['item6']]
        raw_items['Item 6'] = document['10-K'][pos_dat['start'].loc['item6']:pos_dat['start'].loc['item7']]
        raw_items['Item 7'] = document['10-K'][pos_dat['start'].loc['item7']:pos_dat['start'].loc['item7a']]
        raw_items['Item 7A'] = document['10-K'][pos_dat['start'].loc['item7a']:pos_dat['start'].loc['item8']]
        raw_items['Item 8'] = document['10-K'][pos_dat['start'].loc['item8']:pos_dat['start'].loc['item9']]
        raw_items['Item 9'] = document['10-K'][pos_dat['start'].loc['item9']:pos_dat['start'].loc['item10']]
        raw_items['Item 10'] = document['10-K'][pos_dat['start'].loc['item10']:pos_dat['start'].loc['item11']]
        raw_items['Item 11'] = document['10-K'][pos_dat['start'].loc['item11']:pos_dat['start'].loc['item12']]
        raw_items['Item 12'] = document['10-K'][pos_dat['start'].loc['item12']:pos_dat['start'].loc['item13']]
        raw_items['Item 13'] = document['10-K'][pos_dat['start'].loc['item13']:pos_dat['start'].loc['item14']]
        raw_items['Item 14'] = document['10-K'][pos_dat['start'].loc['item14']:pos_dat['start'].loc['item15']]
    except Exception as e:
        with open('./error.txt', "w") as f:
            f.write(e)
        return 'OP FAILED'"""

    for key in raw_items.keys():
        content = BeautifulSoup(raw_items[key], 'lxml')
        raw_items[key] = content.get_text("\n\n")
        raw_items[key] = remove_unicode(raw_items[key])
    
    return raw_items

def count_tokens(text):
    enc = tiktoken.encoding_for_model("gpt-4")
    return len(enc.encode(text))

def gpt_get_10k_sections(user_prompt):
    system_prompt = [
        """The current year is 2023. Only use the function that has been provided to you. """
        """Your job is to pick the right arguments while calling the function 'get_10k_section'. """
        """This function returns the relevant section of a company's 10-K SEC filing based on the user's question. """
        """Here are the definitions of the different sections of the 10-K filing: """
        """1. Item 1A: “Business” requires a description of the company’s business, including its main products """
        """and services, what subsidiaries it owns, and what markets it operates in. This section may also include """
        """information about recent events, competition the company faces, regulations that apply to it, labor """
        """issues, special operating costs, or seasonal factors. This is a good place to start to understand how the company operates. """
        """“Risk Factors” includes information about the most significant risks that apply to the company or to """
        """its securities. Companies generally list the risk factors in order of their importance. In practice, this section """
        """focuses on the risks themselves, not how the company addresses those risks. Some risks may be true for the entire """
        """economy, some may apply only to the company’s industry sector or geographic region, and some may be unique to the company. """
        """2. Item 1B: “Business” requires a description of the company’s business, including its main products """
        """and services, what subsidiaries it owns, and what markets it operates in. This section may also include """
        """information about recent events, competition the company faces, regulations that apply to it, labor """
        """issues, special operating costs, or seasonal factors. This is a good place to start to understand how the company operates. """
        """ “Unresolved Staff Comments” requires the company to explain certain comments it has received from the SEC staff """
        """on previously filed reports that have not been resolved after an extended period of time. Check here to see whether the """
        """SEC has raised any questions about the company’s statements that have not been resolved. """
        """3. Item 2: “ Properties” includes information about the company’s significant physical properties, such as """
        """principal plants, mines and other materially important physical properties. """
        """4. Item 3: “Legal Proceedings” requires the company to include information about significant pending lawsuits or other """
        """legal proceedings, other than ordinary litigation. """
        """5. Item 4: ”Mine Safety Disclosures” requires disclosure, if applicable, of. Information concerning mine safety """
        """violations, among other things. """
        """6. Item 5: “Market for Registrant’s Common Equity, Related Stockholder Matters and Issuer Purchases of Equity Securities” """
        """requires information about the company’s equity securities, including market information, the number of holders """
        """of the shares, dividends, stock repurchases by the company, and similar information. """
        """7. Item 6 This item has no required information, but is reserved by the SEC for future rulemaking.  """
        """Prior to February 2021, however, this item was titled “Selected Financial Data” and  required summarized """
        """financial data about the company for the last five years. """
        """8. Item 7 “Management’s Discussion and Analysis of Financial Condition and Results of Operations” gives the company’s """
        """perspective on the business results of the past financial year. This section, known as the MD&A for short, allows """
        """company management to tell its story in its own words. The MD&A presents: (1) The company’s operations and financial """
        """results, including information about the company’s liquidity and capital resources and any known trends or """
        """uncertainties that could materially affect the company’s results. This section may also discuss management’s """
        """views of key business risks and what it is doing to address them. """
        """(2) Material changes in the company’s results compared to a prior period. """
        """(3) Critical accounting judgments, such as estimates and assumptions. These accounting judgments – and any changes from """
        """previous years – can have a significant impact on the numbers in the financial statements, such as assets, costs, and net income. """
        """9. Item 7A “Quantitative and Qualitative Disclosures about Market Risk” requires information about the company’s exposure to market """
        """risk, such as interest rate risk, foreign currency exchange risk, commodity price risk or equity price risk. The company """
        """may discuss how it manages its market risk exposures. """
        """10. Item 8 “Financial Statements and Supplementary Data” requires the company’s audited financial statements. This includes """
        """the company’s income statement (which is sometimes called the statement of earnings or the statement of operations), balance """
        """sheets, statement of cash flows and statement of stockholders’ equity. The financial statements are accompanied by notes that """
        """explain the information presented in the financial statements. """
        """11. Item 9 “Changes in and Disagreements with Accountants on Accounting and Financial Disclosure” requires a company, if there """
        """has been a change in its accountants, to discuss any disagreements it had with those accountants. Many investors view this """
        """disclosure as a red flag. """
        """12. Item 10 “Directors, Executive Officers and Corporate Governance” requires information about the background and experience of the """
        """company’s directors and executive officers, the company’s code of ethics, and certain qualifications for directors and """
        """committees of the board of directors. """
        """13. Item 11 “Executive Compensation” includes detailed disclosure about the company’s compensation policies and programs and how much """
        """compensation was paid to the top executive officers of the company in the past year. """
        """14. Item 12 “Security Ownership of Certain Beneficial Owners and Management and Related Stockholder Matters” requires """
        """information about the shares owned by the company’s directors, officers and certain large shareholders, and about shares """
        """covered by equity compensation plans. """
        """15. Item 13 “Certain Relationships and Related Transactions, and Director Independence” includes information about relationships """
        """and transactions between the company and its directors, officers and their family members. It also includes information about """
        """whether each director of the company is independent. """
        """16. Item 14 “Principal Accountant Fees and Services” requires companies to disclose the fees they paid to their accounting firm for """
        """various types of services during the year. Although these disclosures are required by the 10-K, most companies meet this """
        """requirement by providing the information in a separate document called the proxy statement, which companies provide to their """
        """shareholders in connection with annual meetings. If the information is provided through the proxy statement, the 10-K would include """
        """a statement from the company that it is incorporating the information from the proxy statement by reference – in effect directing """
        """readers to go to the proxy statement document to find this information. Keep in mind that the proxy statement is typically """
        """filed a month or two after the 10-K. """
        """17. Item 15 “Exhibits, Financial Statement Schedules” requires a list of the financial statements and exhibits included as """
        """part of the Form 10-K. Many exhibits are required, including documents such as the company’s bylaws, copies of its """
        """material contracts, and a list of the company’s subsidiaries."""
    ]

    user_prompt_prefix = [
        """The current year is 2023. Pay attention to the definitions in your system prompt. """
        """Your job is to provide the right arguments required to answer the following question. """
        """Only mention the argument, do not provide any additional details. Here is the question: """
    ]

    functions = [
        {
            "name": "get_10k_section",
            "description": "Pick the relevant section of the SEC 10-K filing",
            "parameters": {
                "type": "object",
                "properties": {
                    "sections": {
                        "type": ["string"],
                        "description": "The sections of the SEC 10-K filing that must be returned, eg, ['Item 1'], ['Item 1', 'Item 7A', 'Item 12'], etc.",
                    },
                },
                "required": ["sections"],
            },
        }
    ]

    response = call_gpt(user_prompt_prefix[0] + user_prompt, system_prompt[0], functions)
    #print(response)

    if response is not None:
        response_message = response["choices"][0]["message"]
        return json.loads(response_message["content"])["sections"]
        #if response_message.get("function_call"):
            #function_args = json.loads(response_message["function_call"]["arguments"])
            #print(function_args['sections'])

            #function_response = get_10k_text(function_args.get("sections"))

def gpt_get_10k_years(user_prompt):
    system_prompt = [
        """The current year is 2023. Your job is to determine the years for which the SEC 10-K filings must be fetched. """
        """You must determine that based on the user's question. Return your answer in a list."""
    ]

    user_prompt_prefix = [
        """The current year is 2023. Pay attention to the system prompt. Your job is to provide the right argument to answer the """
        """following question. Only mention the argument, do not provide any additional details. Here is the question: """
    ]

    functions = [
        {
            "name": "get_10k_year",
            "description": "Pick the relevant years of the SEC 10-K filings to pick.",
            "parameters": {
                "type": "object",
                "properties": {
                    "years": {
                        "type": ["string"],
                        "description": "The year of SEC 10-K filings that must be fetched, eg, ['1999'], ['2010', '2012', '2014'], etc.",
                    },
                },
                "required": ["years"],
            },
        }
    ]

    response = call_gpt(user_prompt_prefix[0] + user_prompt, system_prompt[0], functions)
    #print(response)

    if response is not None:
        response_message = response["choices"][0]["message"]
        return json.loads(response_message["content"])["years"]
        #if response_message.get("function_call"):
        #    function_args = json.loads(response_message["function_call"]["arguments"])
        #    print(function_args['years'])

            #function_response = get_10k_text(function_args.get("sections"))

def gpt_get_10k_companies(user_prompt):
    system_prompt = [
        """The current year is 2023. Your job is to determine the companies for which the SEC 10-K filings must be fetched. """
        """You must determine that based on the user's question."""
    ]

    user_prompt_prefix = [
        """The current year is 2023. Pay attention to the system prompt. Your job is to provide the right argument to answer the """
        """following question. Only mention the argument, do not provide any additional details. Here is the question: """
    ]

    functions = [
        {
            "name": "get_10k_companies",
            "description": "Pick the relevant companies of the SEC 10-K filings to pick.",
            "parameters": {
                "type": "object",
                "properties": {
                    "companies": {
                        "type": ["string"],
                        "description": "The companies of SEC 10-K filings that must be fetched, eg, ['Apple'], ['Apple', 'Netflix', 'Google'], etc.",
                    },
                },
                "required": ["companies"],
            },
        }
    ]

    response = call_gpt(user_prompt_prefix[0] + user_prompt, system_prompt[0], functions)
    #print(response)

    if response is not None:
        response_message = response["choices"][0]["message"]
        return json.loads(response_message["content"])["companies"]
        #if response_message.get("function_call"):
        #    function_args = json.loads(response_message["function_call"]["arguments"])
        #    print(function_args['companies'])

            #function_response = get_10k_text(function_args.get("sections"))
    #return response["choices"][0]["message"]["content"] 

openai.api_key = os.getenv('OPENAI_API_KEY')

# Example dummy function hard coded to return the same weather
# In production, this could be your backend API or an external API
def get_current_weather(location, unit="fahrenheit"):
    """Get the current weather in a given location"""
    weather_info = {
        "location": location,
        "temperature": "72",
        "unit": unit,
        "forecast": ["sunny", "windy"],
    }
    return json.dumps(weather_info)

def call_gpt(user_prompt, system_prompt, functions=None, additional_messages=None):
    messages = [{"role": "system", "content": system_prompt,
                "role": "user", "content": user_prompt}]

    if additional_messages is not None:
        messages.append(additional_messages)
        
    if functions is None:
        response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
    )
    else:
        response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        functions=functions,
        function_call="auto",  # auto is default, but we'll be explicit
    )

    return response

def run_conversation():
    # Step 1: send the conversation and available functions to GPT
    messages = [{"role": "system", "content": "Only use the functions that have been provided to you.",
                "role": "user", "content": "What's the weather like in Boston?"}]
    functions = [
        {
            "name": "get_current_weather",
            "description": "Get the current weather in a given location",
            "parameters": {
                "type": "object",
                "properties": {
                    "location": {
                        "type": "string",
                        "description": "The city and state, e.g. San Francisco, CA",
                    },
                    "unit": {"type": "string", "enum": ["celsius", "fahrenheit"]},
                },
                "required": ["location"],
            },
        }
    ]
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo-0613",
        messages=messages,
        functions=functions,
        function_call="auto",  # auto is default, but we'll be explicit
    )
    response_message = response["choices"][0]["message"]

    # Step 2: check if GPT wanted to call a function
    if response_message.get("function_call"):
        # Step 3: call the function
        # Note: the JSON response may not always be valid; be sure to handle errors
        available_functions = {
            "get_current_weather": get_current_weather,
        }  # only one function in this example, but you can have multiple
        function_name = response_message["function_call"]["name"]
        fuction_to_call = available_functions[function_name]
        function_args = json.loads(response_message["function_call"]["arguments"])
        function_response = fuction_to_call(
            location=function_args.get("location"),
            unit=function_args.get("unit"),
        )

        # Step 4: send the info on the function call and function response to GPT
        messages.append(response_message)  # extend conversation with assistant's reply
        messages.append(
            {
                "role": "function",
                "name": function_name,
                "content": function_response,
            }
        )  # extend conversation with function response
        second_response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo-0613",
            messages=messages,
        )  # get a new response from GPT where it can see the function response
        return second_response

def fetch_text(companies, sections, years):
    text = {}

    for company in companies:
        text[company] = {}
        for year in years:
            text[company][year] = {}

    for company in companies:
        cik = find_cik(company, company)
        if cik == "OP FAILED":
            for year in years:
                for section in sections:
                    text[company][year][section] = 'SECTION UNAVAILABLE'
        else:
            filing_history = get_filing_history(cik)
            for year in years:
                accn = ''
                if 'EntityCommonStockSharesOutstanding' in filing_history['facts']['dei'].keys():
                    for entry in filing_history['facts']['dei']['EntityCommonStockSharesOutstanding']['units']['shares']:
                        if entry['fy'] == int(year) and entry['form'] == '10-K':
                            #print('I AM COMING HERE')                        
                            accn = entry['accn']
                            #print(accn)
                        #if entry['form'] == '10-K':
                        #    print(entry['fy'], entry['accn'])
                elif 'EntityPublicFloat' in filing_history['facts']['dei'].keys():
                    #print(filing_history['facts']['dei']['EntityPublicFloat'])
                    for entry in filing_history['facts']['dei']['EntityPublicFloat']['units']['USD']:
                        if entry['fy'] == int(year) and entry['form'] == '10-K':
                            #print('I AM COMING HERE')                        
                            accn = entry['accn']
                            #print(accn)
                        #if entry['form'] == '10-K':
                            #print(entry['fy'], entry['accn'])
                if accn == '':
                    for section in sections:#raise Exception("RECORD NOT FOUND")
                        text[company][year][section] = 'SECTION UNAVAILABLE'
                    continue
                #print('COMING HERE')
                company_filing = get_company_filing(accn, cik) 
                temp = get_10k_text(company_filing, cik, company)  
                #print(temp.keys())
                for section in sections:
                    text[company][year][section] = temp[section]
    return text

def reduce_context(question, text):
    system_prompt = "Pay attention to what the user says. Your job is to reduce the size of the text and only output the portions of the text \
    that will help you answer the user's question. Do not change or modify the text. Remove anything that is completely unrelated to the question."

    output = ''

    if count_tokens(text) > 8000:
        temp_text = text
        while(len(temp_text) > 17000): # this is the character count, not the token count
            print('coming here', len(temp_text))
            sample = temp_text[:17000 + 200] #adding a 200 so that there's some overlap between consecutive contexts
            temp_text = temp_text[17000:]
            #print('here ', count_tokens(sample))
            user_prompt = f"Your job is to reduce the size of the text and only output the portions of the text \
                that will help you answer this question. Do not change or modify the text or add additional text. Remove anything that is \
                completely unrelated to the question. DO NOT ADD ADDITIONAL TEXT. The output should only contain the requested information. \
                Here is the question: '{question}'. Here is the text: '{sample}'."
            output += llm(user_prompt)
            #output += call_gpt(user_prompt, 'Pay attention to the user prompt.')["choices"][0]["message"]['content']

        if count_tokens(temp_text) > 0:
            sample = temp_text[:17000 + 200]
            #print('last ', count_tokens(sample))
            user_prompt = f"Your job is to reduce the size of the text and only output the portions of the text \
                that will help you answer this question. Do not change or modify the text or add additional text. Remove anything that is \
                completely unrelated to the question. DO NOT ADD ADDITIONAL TEXT. The output should only contain the requested information. \
                Here is the question: '{question}'. Here is the text: '{sample}'."
            output += llm(user_prompt)
            #output += call_gpt(user_prompt, 'Pay attention to the user prompt.')["choices"][0]["message"]['content']
    else:
        user_prompt = f"Your job is to reduce the size of the text and only output the portions of the text \
                that will help you answer this question. Do not change or modify the text or add additional text. Remove anything that is \
                completely unrelated to the question. DO NOT ADD ADDITIONAL TEXT. The output should only contain the requested information. \
                Here is the question: '{question}'. Here is the text: '{text}'."
        output = llm(user_prompt)
        #output += call_gpt(user_prompt, 'Pay attention to the user prompt.')["choices"][0]["message"]['content']

    #response = call_gpt(user_prompt, system_prompt)
    #if response is not None:
    #    output = response["choices"][0]["message"]["content"]
    #else:
    #    output = "FAILED"
    
    return output

def agent(user_prompt):
    companies = gpt_get_10k_companies(user_prompt)
    time.sleep(1)
    years = gpt_get_10k_years(user_prompt)
    time.sleep(1)
    sections = gpt_get_10k_sections(user_prompt)
    time.sleep(1)

    print(companies, sections, years)

    text = fetch_text(companies, sections, years)

    temp_text = ''
    for company in companies:
        for year in years:
            for section in sections:
                temp_text = temp_text + text[company][year][section]
    
    #print(temp_text)
    
    prompt_info = f""
    if count_tokens(temp_text) < 7000:
        #print('I AM HERE.')
        for company in companies:
            for year in years:
                for section in sections:
                    sub_prompt = f"{company}'s 10-K filing section {section} for the year {year}: {text[company][year][section]}"
                    prompt_info = prompt_info + sub_prompt
    else:
        print('Reducing context size...')
        temp_text = ''
        for company in companies:
            for year in years:
                for section in sections:
                    print('reducing context ', count_tokens(text[company][year][section]))
                    #print(text[company][year][section])
                    #time.sleep(5)
                    text[company][year][section] = reduce_context(user_prompt, text[company][year][section])
                    temp_text = temp_text + text[company][year][section]
        # assuming one pass of reducing context will be sufficient
        if count_tokens(temp_text) < 7000:
            for company in companies:
                for year in years:
                    for section in sections:
                        sub_prompt = f"{company}'s 10-K filing section {section} for the year {year}: {text[company][year][section]}"
                        prompt_info = prompt_info + sub_prompt
        else:
            print('Context is still too long.')

    new_user_prompt = f"Using the information below fetched from various SEC 10-K filings, you must answer the following question: {user_prompt}. \
    Use the following information to answer the above question: {prompt_info}"

    system_prompt = "You are a financial expert. You must answer the user's question using the information that has been provided. Only utilize \
    the information mentioned in the user prompt. If the information provided by the user does not answer your question, you must let the user know by \
    saying 'I cannot answer the question'. Do not output anything else in case you cannot answer the question."

    response = call_gpt(new_user_prompt, system_prompt)

    if response is not None:
        output = response["choices"][0]["message"]['content']
    
    #print(output)
    return output

#agent("How was Nvidia's earnings last year?")
#agent("What was Apple's Income before provision for income taxes for 2022?")
#agent("Can you give a summary of the Item 2 of Apple's 2021 10-K filings?")
#gpt_get_10k_companies("How was Nvidia's earnings compared to AMD's?")

#print(count_tokens(companies_list))

