import docker 
from pathlib import Path
import tarfile
from io import BytesIO
import os

class PlotExecutor:
    def __init__(self,logger):
        self.client = docker.from_env()
        self.DATA_DIR = Path.cwd()
        self.container = None
        self.logger = logger
        self.start_docker()

    def start_docker(self):
        image_name = 'plot_executor'
        image_tag = 'latest'
        container_name = 'plot_executor_service'
        host_dir = f'{self.DATA_DIR}/plot'

        if not (self.DATA_DIR / 'plot_code').exists():
            os.makedirs(self.DATA_DIR / 'plot_code')

        if not (self.DATA_DIR / 'plot').exists():
            os.makedirs(self.DATA_DIR / 'plot')

        # Check if the container is already running
        running_containers = self.client.containers.list(filters={'name': container_name})
        if len(running_containers) > 0:
            self.container = running_containers[0]
            self.logger.info(f'Using existing container {container_name}')
        else:
            # Build the image
            self.logger.info(f'{self.DATA_DIR}/config/plot_config')
            image, _ = self.client.images.build(
                path=f'{self.DATA_DIR}/config/plot_config',
                dockerfile='Dockerfile',
                tag=f'{image_name}:{image_tag}',
            )

            # Set up the volume
            volumes = {host_dir:{'bind':'/app/output','mode':'rw'}}

            try: 
                # Run the container
                self.container = self.client.containers.run(
                    image.short_id,
                    auto_remove = True,
                    volumes = volumes,
                    detach = True,
                    name = container_name,
                )
                self.logger.info(f'Created new container {container_name}')
            except Exception as e:
                self.logger.warning(f"error occured in create new container: {e}")

    def create_in_memory_folder_tarball(self, folder_path):
        folder_data = BytesIO()
        with tarfile.open(fileobj=folder_data, mode='w') as tar:
            for root, _, files in os.walk(folder_path):
                for file in files:
                    file_path = os.path.join(root, file)
                    tar.add(file_path, arcname=os.path.relpath(file_path, folder_path))
        folder_data.seek(0)
        return folder_data

    def copy_file_to_container(self, files_path):
        source_folder = files_path
        destination_path = '/app' 

        # Create an in-memory tarball of the folder
        folder_tarball = self.create_in_memory_folder_tarball(source_folder)

        # Copy the folder to the container
        self.container.put_archive(destination_path, folder_tarball)


    def run_plot(self):
        self.copy_file_to_container(f'{self.DATA_DIR}/plot_code')
        exec_command = ['sh', '-c', 'pip install -r requirements.txt && python plot.py']
        exec_output = self.container.exec_run(exec_command)
        self.logger.info(exec_output.output.decode('utf-8'))
        return [exec_output.exit_code,exec_output.output.decode('utf-8')]

