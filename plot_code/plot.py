
import yfinance as yf
import matplotlib.pyplot as plt

# Get the data for AMD and Intel stocks
amd = yf.download("AMD", start="2019-11-01", end="2021-11-01")
intel = yf.download("INTC", start="2019-11-01", end="2021-11-01")

# Plot the closing prices for AMD and Intel stocks
plt.plot(amd['Close'], label='AMD')
plt.plot(intel['Close'], label='Intel')

# Set the title and labels for the plot
plt.title('AMD vs Intel Stock Price (2 Year)')
plt.xlabel('Date')
plt.ylabel('Closing Price')

# Add a legend to the plot
plt.legend()

# Display the plot
plt.show()

plt.savefig('output/plot.png')